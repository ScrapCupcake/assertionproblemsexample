﻿using UnityEngine;
using System.Collections;

public class DisableTarget : MonoBehaviour {

	public GameObject target;
	public float delay = 0.5f;
	float timeToDisable;

	// Use this for initialization
	void Start () {
		timeToDisable = Time.time + delay;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time >= timeToDisable) {
			target.SetActive(false);
		}
	}
}
