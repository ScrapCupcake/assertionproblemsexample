# README #

A very small Unity project I made to demonstrate some issues I'm having with the Unity Test Tools, trying to get my understanding of what should cause positive and negative results with the Assertion component, so I can apply them correctly to more complex projects. I'll update this readme with lessons learned.

Question one: What should cause the Assertion component, when set to test isRenderedByCamera, to pass for the compare type IsNotVisible? I have two tests, one that only tests if a cube in scene is visible, at start and after 1 second, and a second test with a script that calls IsActive(false) on the cube after 0.5 seconds, and asserts IsNotVisible on it after 1 second. The first test passes as expected, the second fails. I don't understand :(

Hunter Thomas
@scrapcupcake
scrapcupcake@gmail.com